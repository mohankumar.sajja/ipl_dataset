"""
Story on Mumbai Indian Wins
"""
# from file_reader import deliveries_file_function
# from file_reader import match_file_function

# MATCH_DATA = match_file_function("ipl_data/matches.csv")

import csv
import collections
import matplotlib.pyplot as plt

MATCH_FILE = open("ipl_data/matches.csv", 'r', encoding='utf-8')
MATCH_DATA = csv.DictReader(MATCH_FILE)

def dict_sort(data):
    """
    return sorted dictionary
    """
    return collections.OrderedDict(sorted(data.items()))

def add_data_to_given_dict(item, dictionary):
    """
    Add item to dictionary
    """
    if item in dictionary:
        dictionary[item] += 1
    else:
        dictionary[item] = 1

def extract():
    """
    Extract data from given data sets
    """
    matches_won = {}
    team = "Mumbai Indians"
    toss_won = {}
    batting_first = {}
    bowling_first = {}
    player_of_match = {}
    for match in MATCH_DATA:
        if match["winner"] == team:
            season = match["season"]
            # Get data for matches won every year
            add_data_to_given_dict(season, matches_won)
            # Get data for when toss win
            toss = match["toss_winner"]
            toss_decision = match['toss_decision']
            if toss == team:
                add_data_to_given_dict(season, toss_won)
            # Get data when batting first/fileding first
                if toss_decision == "bat":
                    add_data_to_given_dict(season, batting_first)
                else:
                    add_data_to_given_dict(season, bowling_first)
            else:
                if toss_decision == "bat":
                    add_data_to_given_dict(season, bowling_first)
                else:
                    add_data_to_given_dict(season, batting_first)

            # extract player of the match data
            add_data_to_given_dict(match["player_of_match"], player_of_match)

    return (dict_sort(matches_won), dict_sort(toss_won),
            dict_sort(batting_first), dict_sort(bowling_first),
            collections.OrderedDict(sorted(player_of_match.items(),
                                           key=lambda kv: kv[1], reverse=True)[:10]))

def display_plot(data, xlabel, ylabel, title):
    """
    Display bar plot for given data
    """
    x_axis = data.keys()
    y_axis = data.values()

    plt.bar(x_axis, y_axis, label="Mumbai Indians", color="mediumorchid")
    plt.legend()
    if len(list(x_axis)[0]) > 5:
        plt.xticks(rotation="30", fontsize="6")
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.title(title)
    plt.show()

def plot(matches_won, toss_won, batting_first, bowling_first, player_of_match):
    """
    get and prepare data to plot
    """
    display_plot(matches_won, "years", "Total Wins", "Mumbai Indians wins year wise")
    display_plot(toss_won, "years", "wins", "Mumbai Indians wins when toss won")
    display_plot(batting_first, "years", "Wins", "MI won when batting first")
    display_plot(bowling_first, "years", "Wins", "MI won when bowling first")
    display_plot(player_of_match, "Player", "Number of times", "MI Top-10 Player of Match winners")

def caller():
    """
    function call starts
    """
    plot(*extract())

caller()

MATCH_FILE.close()
