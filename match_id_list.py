import csv

MATCH_FILE = open("ipl_data/matches.csv", 'r', encoding='utf-8')
MATCH_DATA = csv.DictReader(MATCH_FILE)

def match_ids(year):
    match_ids_list = []
    for match in MATCH_DATA:
        if match['season'] == year:
            match_ids_list.append(match['id'])
    return match_ids_list
