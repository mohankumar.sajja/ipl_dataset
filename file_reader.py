import csv

def deliveries_file_function(filename):
    deliveries_file = open(filename, 'r', encoding = 'utf-8')
    deliveries_data = csv.DictReader(deliveries_file)
    yield deliveries_data
    deliveries_file.close()


def match_file_function(filename):
    match_file = open(filename, 'r', encoding = 'utf-8')
    match_data = csv.DictReader(match_file)
    yield match_data
    match_file.close()

