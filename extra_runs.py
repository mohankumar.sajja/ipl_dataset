""" Get extra runs by every team in year 2016 """

import csv
import match_id_list
import matplotlib.pyplot as plt


DELIVERIES_FILE = open("ipl_data/deliveries.csv", 'r', encoding='utf-8')
DELIVERIES_DATA = csv.DictReader(DELIVERIES_FILE)
MATCH_FILE = open("ipl_data/matches.csv", 'r', encoding='utf-8')
MATCH_DATA = csv.DictReader(MATCH_FILE)

def extract():
    """
    Extract data from given data sets
    """
    # match_ids_list = []
    # for match in MATCH_DATA:
    #     if match['season'] == '2016':
    #         match_ids_list.append(match['id'])
    match_ids_list = match_id_list.match_ids('2016')

    extra_runs_by_each_team = {}
    for delivery in DELIVERIES_DATA:
        bowling_team = delivery['bowling_team']
        extra_runs = int(delivery['extra_runs'])
        if delivery['match_id'] in match_ids_list:
            if bowling_team in extra_runs_by_each_team:
                extra_runs_by_each_team[bowling_team] += extra_runs
            else:
                extra_runs_by_each_team[bowling_team] = extra_runs
    return extra_runs_by_each_team

def plot(extra_runs_by_each_team):
    """
    Plots extracted data
    """
    teams = extra_runs_by_each_team.keys()
    teams_short_names = []
    for team in teams:
        teams_short_names.append("".join([word[0] for word in team.split()]))
    extra_runs = extra_runs_by_each_team.values()
    plt.bar(teams_short_names, extra_runs, label='Extra runs', color='mediumorchid')
    plt.legend()
    plt.xlabel('Teams')
    plt.ylabel('Extra Runs')
    plt.title('Extra Runs by Teams in 2016')
    plt.show()

def caller_method():
    """
    calls functions
    """
    plot(extract())


caller_method()

DELIVERIES_FILE.close()
MATCH_FILE.close()
