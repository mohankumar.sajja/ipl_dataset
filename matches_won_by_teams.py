"""
Matches won by team by every year
"""

import csv
import copy
import matplotlib.pyplot as plt


CSV_DATA = open("ipl_data/matches.csv", 'r', encoding='utf-8')
MATCH_DATA = csv.DictReader(CSV_DATA)

# def team_include(team_wins_per_year, team, years):
#     """ Assign default values for every team """
#     team_wins_per_year[team] = copy.deepcopy(years)

def extract():
    """Extract data from match data to plot the data"""
    # years = {}
    # team_wins_per_year = {}
    # for match in MATCH_DATA:
    #     # extracting all seasons list and storing in years
    #     season = match['season']
    #     if season not in years:
    #         years[season] = 0
    # CSV_DATA.seek(0)
    # next(MATCH_DATA)
    # for match in MATCH_DATA:
    #     # extracting all team names and assigning them years
    #     team_include(team_wins_per_year, match['team1'], years)
    #     team_include(team_wins_per_year, match['team2'], years)
    # CSV_DATA.seek(0)
    # next(MATCH_DATA)
    # for match in MATCH_DATA:
    #     winner = match['winner']
    #     season = match['season']
    #     if winner in team_wins_per_year:
    #         team_wins_per_year[winner][season] += 1
    # print(sorted(team_wins_per_year.items()))
    # return years, sorted(team_wins_per_year.items())
    data = {}
    
    for match in MATCH_DATA:
        if match["season"] in data:
            if match["winner"] in data[match["season"]]:
                data[match["season"]][match["winner"]] += 1
            else:
                data[match["season"]][match["winner"]] = 1
        else:
            data[match["season"]] = {}
    # print(data)
    return data


def plot(data):
    """
    Plot extracted data
    """
    bottom = [0] * len(data)
    years = sorted(data.keys())
    teams = set()
    for season in data:
        teams |= data[season].keys()
    teams.remove('')
    # print(years)
    teams = sorted(teams)
    # print(teams)
    for team in teams:
        wins_per_year = []
        for season in years:
            if team in data[season]:
                wins_per_year.append(data[season][team])
            else:
                wins_per_year.append(0)
        plt.bar(years, wins_per_year, label=team, bottom=bottom)
        plt.legend(teams, loc=2, prop={'size': 5})
        bottom = [bottom[index]+wins_per_year[index] for index in range(len(bottom))]
    plt.ylim(0, 100)
    plt.xticks(years)
    plt.xlabel('Years')
    plt.ylabel('Matches')
    plt.title('Matches won by teams')
    plt.show()

def caller():
    """ Function calls start here """
    plot(extract())
caller()
CSV_DATA.close()
