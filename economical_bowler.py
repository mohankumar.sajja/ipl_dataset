"""
Get top 10 economical bowlers
"""

import csv
import match_id_list
import collections
import matplotlib.pyplot as plt

DELIVERIES_FILE = open("ipl_data/deliveries.csv", 'r', encoding='utf-8')
DELIVERIES_DATA = csv.DictReader(DELIVERIES_FILE)
MATCH_FILE = open("ipl_data/matches.csv", 'r', encoding='utf-8')
MATCH_DATA = csv.DictReader(MATCH_FILE)

def extract():
    """
    Extract data from given data sets
    """
    match_ids_list = match_id_list.match_ids('2015')

    #Getting Bowlers Names without duplicates and assigning totals for each bowlers
    bowler_data = {}
    for delivery in DELIVERIES_DATA:
        name = delivery['bowler']
        if delivery['match_id'] in match_ids_list and delivery['is_super_over'] == '0':
            if name not in bowler_data:
                bowler_data[name] = {'total_balls':0, 'total_runs_given':0}

    DELIVERIES_FILE.seek(0)
    next(DELIVERIES_DATA)

    # #Getting total runs and total balls for each bowler
    for delivery in DELIVERIES_DATA:
        bowler_name = delivery['bowler']
        if delivery['match_id'] in match_ids_list and delivery['is_super_over'] == '0':
            bowler_data[bowler_name]['total_balls'] += 1
            bowler_data[bowler_name]['total_runs_given'] += (int(delivery['total_runs'])-
                                                             int(delivery['bye_runs'])-
                                                             int(delivery['legbye_runs'])-
                                                             int(delivery['penalty_runs']))
    # #Calculating economies for each bowler
    economies = {}
    for bowler in bowler_data:
        overs = bowler_data[bowler]['total_balls']/6
        economy = bowler_data[bowler]['total_runs_given']/overs
        economies[bowler] = economy
    return collections.OrderedDict(sorted(economies.items(), key=lambda kv: kv[1]))

def plot(economies):
    """
    Plots extracted data
    """
    names_list = list(economies.keys())
    economies_list = sorted(economies.values())
    plt.barh(names_list[0:10], economies_list[0:10], label="Economies", color="mediumorchid")
    plt.legend()
    plt.ylabel("Bowlers")
    plt.xlabel("Economy")
    plt.title("Top 10 Bowler's Economies")
    plt.show()

def caller_method():
    """
    start call functions
    """
    plot(extract())


caller_method()

DELIVERIES_FILE.close()
MATCH_FILE.close()
