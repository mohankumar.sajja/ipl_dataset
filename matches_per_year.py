"""
Matches per every year
"""
import csv
import matplotlib.pyplot as plt


def main():
    """ Main function """
    csv_file = open("ipl_data/matches.csv", 'r', encoding='utf-8')
    csv_data = csv.DictReader(csv_file)
    years = set()
    matches_per_year = {}
    for match in csv_data:
        years.add(match["season"])
        if match['season'] in matches_per_year:
            matches_per_year[match['season']] += 1
        else:
            matches_per_year[match['season']] = 1
    sorted_by_years = sorted(matches_per_year.items())
    total_matches = [item[1] for item in sorted_by_years]
    plt.bar(sorted(list(years)), total_matches, label='Matches per Year', color='lightseagreen')
    plt.legend()
    plt.xlabel('Years')
    plt.ylabel('Matches')
    plt.title('Bar Plot')
    plt.ylim(0, 80)
    plt.show()
    csv_file.close()

if __name__ == "__main__":
    main()
